var express = require('express');
var router = express.Router();

const creationalPatterns = [
  "Abstract Factory",
  "Builder",
  "Factory Method",
  "Prototype",
  "Singleton"
];

const structuralPatterns = [
  "Adapter (Class Inheritance)",
  "Adapter (Object Composition)",
  "Bridge",
  "Composite",
  "Decorator",
  "Facade",
  "Flyweight",
  "Proxy"
];

const behavioralPatterns = [
  "Chain of responsibility",
  "Command",
  "Interpreter",
  "Iterator",
  "Mediator",
  "Memento",
  "Observer",
  "State",
  "Strategy",
  "Template Method",
  "Visitor"
];

const patterns = creationalPatterns.concat(structuralPatterns).concat(behavioralPatterns);
const remainingPatterns = patterns.slice(0);

const students = {
  stefan: [],
  simon: [],
  marcel: [],
  keerthikan: []
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Random GoF Pattern Picker', students, patterns, remainingPatterns });
});

router.post("/pickGoF", (req, res, next) => {
  if(students.hasOwnProperty(req.body.student)){
    if(students[req.body.student].length < 6){
    const random = Math.floor(Math.random() * remainingPatterns.length);
    students[req.body.student].push(remainingPatterns[random]);
    remainingPatterns.splice(random, 1);
    }
  }
  res.redirect("/");
});

module.exports = router;
